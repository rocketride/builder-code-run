
declare module 'gi://Ide?version=47' {
    const Ide47: typeof import('./ide-47.js').default;
    export default Ide47;
}

declare module 'gi://Ide' {
    import Ide47 from './ide-47.js';
    export default Ide47;
}

