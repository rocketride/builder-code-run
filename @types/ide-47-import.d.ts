
type Ide47 = typeof import('./ide-47.js').default;

declare global {
    export interface GjsGiImports {
        Ide: Ide47;
    }
}

export default GjsGiImports;


