import GObject from 'gi://GObject';
import Gtk from 'gi://Gtk?version=4.0';
import GLib from 'gi://GLib';
import Gio from 'gi://Gio';
import Ide from 'gi://Ide';
// @ts-ignore types bug
import Panel from 'gi://Panel';
import Vte from 'gi://Vte?version=2.91'
import System from 'system';


// there is convention that compiler creates 'main' executable
// look into using something like 'main.cpp.exe' for executable name 
// if language is compiled, run_command returns main
// if language is interpreted, run_command returns runner + args + source
interface LanguageRunConfig {
    display_name: string;
    extensions: (string)[];
    type: "Interpreted" | "Compiled";
    run_args: (string)[] | null;
    compiler_args: (string)[] | null;
    compiler: string | null;
    runner: string | null;
}

var gresource = Gio.resource_load("/home/dell/.local/share/gnome-builder/plugins/CodeRun/code_run.gresource");
Gio.resources_register(gresource);
// console.log(GLib.get_current_dir());
// console.log(System.programPath);

function get_compiler_command (config: LanguageRunConfig, source: string): string | null {
    if (config.compiler === null || config.type == "Interpreted") {
        return null;
    }
    var res: string = config.compiler + " ";
    if (config.compiler_args != null) {
        config.compiler_args.forEach(item => {
            res += item + " ";
        });
    }
    return res + source + " ";
}

function get_run_command (config: LanguageRunConfig, source: string): string | null {
    switch (config.type) {
        case "Interpreted":
            var res: string;
            if (config.runner === null) {
                print("Config doesn't have runner");
                return null;
            } else {
                res = config.runner + " ";
            }

            if (config.run_args != null) {
                config.run_args.forEach(item => {
                    res += item + " ";
                });
            }
            return res + source + " ";
            break;

        case "Compiled":
            return "./main "
            break;
    }
}

var language_configs: LanguageRunConfig[] = [
    {
        display_name: "C",
        extensions: ["c"],
        type: "Compiled",
        run_args: null,
        compiler_args: ["-g", "-o main"],
        compiler: "gcc",
        runner: null,
    },
    {
        display_name: "C++",
        extensions: ["cpp"],
        type: "Compiled",
        run_args: null,
        compiler_args: ["-g", "-o main", "-std=c++20"],
        compiler: "g++",
        runner: null,
    },
    {
        display_name: "Prolog",
        extensions: ["pro", "prolog"],
        type: "Interpreted",
        run_args: null,
        compiler_args: null,
        compiler: null,
        runner: "swipl",
    }
]

// collect all supported extensions for faster detection
var ext_map = new Map<string, number>();
for (let i = 0; i < language_configs.length; i++) {
    language_configs[i].extensions.forEach(ext => {
        // would be nice to have different configs for one extension
        // for example: clang++ and g++ for .cpp files
        // or debug and release args
        if (ext_map.has(ext)){
            print("Found duplicate extension");
            return;
        }
        ext_map.set(ext, i);
    });
}

/*
    this class provides widget with run and cancel buttons
    user of this class must connect buttons himself
    user must also notify this class about state
changes of run process
    class takes care of doing appropriate transition
animation
*/
var CodeRunControlButton = GObject.registerClass({
    Template: "resource:///plugins/code_run/code_run_control_button.ui",
    Children: ["run_button", "cancel_button"],
    InternalChildren: ["control_stack"],
    Properties: {
        'is-supported-ext': GObject.ParamSpec.boolean(
            'is-supported-ext',
            'is supported file extension',
            'Is currently selected document has supported extension',
            GObject.ParamFlags.READWRITE,
            false
        ),
        'is-current-running': GObject.ParamSpec.boolean(
            'is-current-running',
            'Is current file running',
            'is currently selected document already running',
            GObject.ParamFlags.READWRITE,
            false
        ),
        'is-running': GObject.ParamSpec.boolean(
            'is-running',
            'Is any file running',
            'is currently selected document already running',
            GObject.ParamFlags.READWRITE,
            false
        ),
    },
    GTypeName: "CodeRunControlButton",
},
class CodeRunControlButton extends Gtk.Revealer {
    declare run_button: Gtk.Button;
    declare cancel_button: Gtk.Button;
    declare private _control_stack: Gtk.Stack;
    declare isRunning: boolean;
    declare isSupportedExt: boolean;
    
    // is currently selected document has supported extension
    declare private _isSupportedExt: boolean;
    // is this document already running
    // currently useless
    declare private _isCurrentRunning: boolean;
    // is any document currently running
    declare private _isRunning: boolean;

    constructor(constructorProperties = {}){
        super(constructorProperties);
        this.bind_property('reveal-child', this, 'is-supported-ext',
            GObject.BindingFlags.BIDIRECTIONAL | GObject.BindingFlags.SYNC_CREATE);
        this.connect("notify::is-running", (self, some_object: boolean) => {
            print("obj: ", self, "object: ", some_object);
            if (this.isRunning == true) {
                this._control_stack.set_visible_child(this.cancel_button);
            } else {
                this._control_stack.set_visible_child(this.run_button);
            }
        });
    };

    on_state_changed () {
        
    }
});

var CodeRunPane = GObject.registerClass({
    Template: "resource:///plugins/code_run/code_run_pane.ui",
    Children: ["build_term", "run_term"],
    InternalChildren: ["term_stack"],
    GTypeName: 'CodeRunPane',
},
class CodeRunPane extends Ide.Pane {
    declare run_term: Ide.TerminalPage;
    declare build_term: Ide.TerminalPage;
    declare _term_stack: Gtk.Stack;

    constructor(constructProperties = {}) {
        super(constructProperties);
        this.run_term!.set_pty(Ide.pty_new_sync());
        this.build_term!.set_pty(Ide.pty_new_sync());
        print("constructor happened");
    } 

    show_build_term () {
        // cant assign TerminalPage to Widget. def a bug
        // @ts-ignore
        this._term_stack.set_visible_child(this.build_term);
        this.build_term.raise();
    }

    show_run_term () {
        // @ts-ignore
        this._term_stack.set_visible_child(this.run_term);
        this.run_term.raise();
    }
});


export var CodeRunAddin = GObject.registerClass({
    Implements: [Ide.WorkspaceAddin],
}, class CodeRunAddin extends GObject.Object {
    workspace?: Ide.Workspace = undefined;

    control_button = new CodeRunControlButton();
    run_pane = new CodeRunPane();    

    context: Ide.Context|null = null;
    podman_runtime?: Ide.Runtime = undefined;
    source: Gio.File|null = null;
    run_cancellable: Gio.Cancellable = new Gio.Cancellable();
    // maybe allow running multiple files at a time?
    // is_running: boolean = false;

    constructor() {
        super();

        // Titlebar button UI:
        this.control_button.run_button.connect("clicked", this.on_run_clicked.bind(this));
        this.control_button.cancel_button.connect("clicked", () => {
            this.run_cancellable.cancel();
            this.control_button.isRunning = false;
        });
    }

    compiled_cb (process: Ide.Subprocess, res: Gio.AsyncResult, source: Gio.File, config: LanguageRunConfig) {
        var cwd = source.get_parent()!;
        var source_rel = cwd.get_relative_path(source)!;
        var run_command = get_run_command(config, source_rel);
        
        if (run_command !== null) {
            // running starts here
            this.run_pane.show_run_term();
                        
            var runner = new Ide.RunContext();

            // if runtime was changed during compile stage it will differ
            // here. take a look if it's a problem later
            // UPDATE: after recent fixes it should be ok 
            this.podman_runtime!.prepare_to_run(null, runner);

            runner.set_cwd(cwd.get_path()!);
            runner.append_args_parsed(run_command);

            // this gets rid of previous terminal content, but I can't 
            // figure out how to preserve it
            var new_pty = Ide.pty_new_sync();
            this.run_pane.run_term!.set_pty(new_pty);
            runner.set_pty(new_pty);

            this.run_pane.run_term!.feed(run_command + "\r\n");
            var run_process = runner.spawn();
            run_process.wait_async(this.run_cancellable, () => {
                GLib.idle_add(GLib.PRIORITY_LOW, () => {
                    this.run_pane.run_term!.feed("Program exited.\r\n");
                    this.control_button.isRunning = false;
                    return GLib.SOURCE_REMOVE;
                });
            });
        } else {
            print("get_run_command returned null");
        }
    }

    on_run_clicked () {
        if (this.podman_runtime !== undefined && this.source?.get_path() !== null) {
            // UI schanges
            this.control_button.isRunning = true;
            //

            // i couldn't find how to clone object in gjs. lets do this
            var source = Gio.File.new_for_path(this.source?.get_path()!);

            let builder = new Ide.RunContext();
            var cwd = source.get_parent()!;
            var source_rel = cwd.get_relative_path(source)!;
            var basename = source.get_basename();
            var extension: string = basename?.split(".").pop()!;

            var config = language_configs[ext_map.get(extension)!];

            var compile_command = get_compiler_command(config, source_rel);
            if (compile_command !== null) {
                // building starts here
                this.run_pane.show_build_term();
                
                this.podman_runtime.prepare_to_build(null, builder);
                builder.set_cwd(cwd.get_path()!);
                print(`Compiler cwd: \"${builder.get_cwd()}\"`);
                // runner.append_argv("lsb-release");
                // runner.append_argv("-i");
                builder.append_args_parsed(compile_command);

                var new_pty = Ide.pty_new_sync();
                this.run_pane.build_term!.set_pty(new_pty);
                builder.set_pty(new_pty);
                    
                try {
                    var process = builder.spawn();
                    this.run_cancellable = new Gio.Cancellable();
                    process.wait_async(this.run_cancellable, (process, res) => {
                        if (!this.run_cancellable.is_cancelled()) {
                            this.compiled_cb(process, res, source, config);
                        } 
                        /* then compilation was cancelled. state should already
                         be changed for us */
                    });
                } catch (e: any) {
                    print(e.message);
                }
            }
        }
    }

    vfunc_load(_workspace: Ide.Workspace) {
        this.workspace = _workspace;
        if (this.workspace.context?.has_project() == false) {
            this.vfunc_unload(_workspace);
            return;
        } else {
            print("Project is be loaded");
        }

        var pos = new Panel.Position();
        pos.set_area(Panel.Area.BOTTOM);
        _workspace.add_pane(this.run_pane, pos);

        var titlebar = this.workspace?.get_header_bar();
        // @ts-ignore
        titlebar?.add(Ide.HeaderBarPosition.RIGHT_OF_CENTER, 1, this.control_button);
        this.context = _workspace.get_context();
        var runtime_manager = Ide.RuntimeManager.from_context(this.context!);

        var fd = runtime_manager.connect("items-changed", this.on_runtimes_changed.bind(this));

        print("fd = ", fd);

        print("Workspace addin loaded");
    }

    on_runtimes_changed(self: Gio.ListModel, position: number, removed: number, added: number) {
        for (var i = position; i < position + added; i+=1) {
            // @ts-ignore
            var item: Ide.Runtime = self.get_item(i)!;
            if (item.id?.startsWith("podman:")) {
                print (`New runtime appears: \"${item.name}\"`);
                if (item.name == "dbx-arch") {
                    print("Found desired runtime");
                    this.podman_runtime = item;
                }
            }
        }
    }

    vfunc_unload(workspace: Ide.Workspace) {
        print("Workspace addin unloaded");
    }

    vfunc_page_changed(some_page: Ide.Page) {
        // page is changed to null after hitting close button on tab
        // @ts-ignore $gtype not found
        if (some_page !== null && some_page.constructor.$gtype === Ide.EditorPage.$gtype) {
            print("Editor page");
            var editor_page: Ide.EditorPage = some_page as Ide.EditorPage;
            var source_file: Gio.File = editor_page.get_file();
            var extension = source_file.get_basename()?.split('.').pop();

            print("Current extension:", extension);
            if (extension !== undefined && ext_map.has(extension)) {
                this.control_button.isSupportedExt = true;
                this.source = source_file;
            } else {
                this.control_button.isSupportedExt = false;
                print("Unsupported extension");
                // print("is running: ", this.is_running);
            }
        }
        print("on_page_changed end");
    }
});
